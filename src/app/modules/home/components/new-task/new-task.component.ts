import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConnectionServices } from '@service/connection-services';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent {
  //formulario para nueva tarea
  formNewTask: FormGroup;

  /**
   * ## Constructor:
   *
   * Se crea formulario con los campos requeridos
   * @param formBuilder
   */
  constructor(
    private connectionServices: ConnectionServices,
    private formBuilder: FormBuilder
  ) {
    this.createForm();
  }

  /**
   * 
   */
  createForm(){    
    this.formNewTask = this.formBuilder.group({
      title: ['', Validators.required],
      state: [false],
    });
  }
  /**
   * ## Create Task
   *
   * Emite el evento con la información del formulario al componente padre
   * y reinicializa los valores del formulario
   */
  createTask(){
    const date = new Date();
    const payload = this.formNewTask.value
    const newProduct = {
      createdAt: date.getFullYear().toString(),
      ...payload,
    };
    this.connectionServices.createTask(newProduct).subscribe(task =>{
      console.log('Una nueva tarea ha sido creada: ',task)
      this.connectionServices.changesValueNewTaskEvent(newProduct);
    });
    this.formNewTask.reset();
  }

}
