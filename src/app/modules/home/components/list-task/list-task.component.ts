import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ConnectionServices } from '@service/connection-services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.scss']
})
export class ListTaskComponent implements AfterViewInit, OnDestroy, OnInit {
  // Listado de tareas obtenidas desde el Endpoint
  listData: any;
  //services
  services: Subscription;
  //variable BehaviorSubject
  servicesBS: Subscription;

  constructor(private connectionServices: ConnectionServices) {
    this.servicesBS = this.connectionServices.newTaskEvent.subscribe(response => {
      if (!response) return
      else this.getTask();
    })
  }

  /**
   * ## ngOnInit
   * 
   * Ejecutará el método getTask() para agregar la lista de tareas
   * en la tabla principal
   */
  ngOnInit(): void { this.getTask(); }

  /**
   * ## ngAfterViewInit
   *
   * Se realiza la consulta al servicio, para cargar la lista de tareas
   */
  ngAfterViewInit(): void { this.getTask(); }

  /**
   * ## getTask
   *
   * consultamos el servicio en búsqueda de tareas
   */
  getTask() {
    this.services = this.connectionServices.getAllTask()
      .subscribe(response => {
        if (!response) return
        else this.listData = response
      })
  }

  /**
   * ## Remove
   *
   * Elimina item del listado de tareas, enviados por el home
   * @param id - String
   */
  remove(id: string) {
    let elemento = this.listData.find(el => el.id === id)
    if (elemento == -1) return
    this.connectionServices.deleteTask(id)
      .subscribe(response => {
        if (!response) return
        else {
          console.log('La tarea ha sido eliminada: ', elemento)
          this.getTask();
        }
      });
  }

  /**## Update
   *
   * Actualiza item del listado de tareas segun estado completado
   * @param id - String
   * @returns
   */
  updateState(id: string) {
    let elemento = this.listData.find(el => el.id === id)
    if (elemento == -1) return
    else elemento.state = !elemento.state
    this.connectionServices.updateTask(id, elemento).subscribe(() => {
      console.log('se actualizo elemento: ', elemento)
    })
  }

  /**
   * ## ngOnDestroy
   *
   * destruimos el servicio
   */
  ngOnDestroy(): void {
    this.services.unsubscribe();
    this.servicesBS.unsubscribe();
  }
}
