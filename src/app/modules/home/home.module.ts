import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { ListTaskComponent } from './components/list-task/list-task.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NewTaskComponent } from './components/new-task/new-task.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    HomeComponent,
    ListTaskComponent,
    NavbarComponent,
    NewTaskComponent,
    RepositoryComponent,
    SearchComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ]
})
export class HomeModule { }
