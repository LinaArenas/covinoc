import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

const api = environment.serverUrl;

@Injectable({
  providedIn: 'root'
})
export class ConnectionServices {
  // Variable de tipo BehaviorSubject
  newTaskEvent:  BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(public http: HttpClient) { }
  
  /**
   * ## Get All Task
   *
   * Servicio que obtiene la lista completa de tareas
   *
   * @example
   *
   * ```
   * [
   *   {
   *       "createdAt":1652787594,
   *       "state":true,
   *       "title":"tarea1",
   *       "id":"1"
   *   },
   *   ...
   * ]
   * ```
   */
  getAllTask():Observable<any>{
    return this.http.get(api);
  }

  /**
   * ## Get Task
   *
   * Servicio que obtiene una tarea específica
   *
   * @param idTask - string: identificador de la tarea
   *
   * @example
   * ```
   * {
   *    "createdAt":1652787594,
   *    "state":true,
   *    "title":"tarea1",
   *    "id":"1"
   * }
   * ```
   * @param id - String
   */
  getTask(id:any):Observable<any>{
    return this.http.get(`${api}:/${id}`);
  }

  /**
   * ## Create Task
   *
   * Servicio que envia una data al Endpoint para la creación de una nueva tarea
   * @param data - Objeto
   */
  createTask(data:any):Observable<any>{
    return this.http.post(api, data);
  }

  /**
   * ## Update Task
   *
   * Servicio que actualiza una tarea
   * @param payload - Objeto
   * @param id -String
   * @returns
   */
  updateTask(id:string, payload:any){
    return this.http.put(`${api}/${id}`, payload);
  }

  /**
   * ## Delete
   *
   * Servicio que elimina tarea a partir del ID
   * @param id
   * @returns
   */
  deleteTask(id:string):Observable<any>{
    return this.http.delete(`${api}/${id}`)
  }

  /**
   * ## Changes Value
   * 
   * Método que modificará el estado de la variable [newTaskEvent], y de ese modo
   * emitir un evento que el componente [list-task] pueda escuchar; en caso de emitirse
   * el evento este ejecutará un método X, que permitirá realizar una nueva consulta de la lista de tareas
   * y cargar los datos nuevos en la tabla principal [list-task]
   * 
   * @param msg - objeto: nueva tarea creada
   */
  changesValueNewTaskEvent(msg:any){
    this.newTaskEvent.next(msg);
  }
}
