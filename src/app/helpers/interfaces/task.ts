export interface Task{
    createdAt:number,
    state:boolean,
    title:string,
    id:string
}